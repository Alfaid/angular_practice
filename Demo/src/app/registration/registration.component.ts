import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.css'
})
export class RegistrationComponent implements OnInit{

  empId   : any;
  empName : any;
  salary  : any;
  gender  : string = '';
  doj     : any;
  country : string ='';
  emailId : any;
  password: any;
  deptId  : string = '';
  countries : any;




  constructor(private service: EmpService){

  }

  ngOnInit() {

    this.service.getAllCountries().subscribe((data: any) =>{
      this.countries = data;
      console.log(data);
    });
  }


  registrationSubmit(registrationForm:any){
    alert("register successfully");
    console.log(registrationForm);
  }


  submit() {
    alert("EmpId: " + this.empId + "\nEmpName: " + this.empName+ "\nSalary:" + this.salary+ "\nGender:" + this.gender + "\nDOJ:" + this.doj + "\nCountry:" + this.country + "\nEmailId:" + this.emailId +  "\nPassword:" + this.password + "\nDeptId: " + this.deptId );
    console.log(this.empId);
    console.log(this.empName);
    console.log(this.salary);
    console.log(this.gender);
    console.log(this.doj);
    console.log(this.country);
    console.log(this.emailId);
    console.log(this.password);
    console.log(this.deptId);
  }

}
