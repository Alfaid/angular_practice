import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: any;
  constructor(private http: HttpClient) {
    this.loginStatus = false;
   }

  setLoginStatus() {
    this.loginStatus = true;
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  setLogoutStatus() {
    this.loginStatus = false;
  }

  getAllCountries(): any {
     return this.http.get('https://restcountries.com/v3.1/all');
  }




}
