import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private service: EmpService){
    this.service.setLogoutStatus();
    this.router.navigate(['login']);
  }



  ngOnInit() { 
  }

}
