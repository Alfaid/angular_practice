import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {
  empId : any;
  emp : any;
  employees : any;

  constructor(){
    this.employees = [
      {empId:101, empName:'Harsha',  salary:1212.12, gender:'Male',   doj:'05-25-2018', country:"IND", emailId:'harsha@gmail.com',  password:'123'},
      {empId:102, empName:'Pasha',   salary:2323.23, gender:'Male',   doj:'06-26-2017', country:"USA", emailId:'pasha@gmail.com',   password:'123'},
      {empId:103, empName:'Indira',  salary:3434.34, gender:'Female', doj:'07-27-2016', country:"CHI", emailId:'indira@gmail.com',  password:'123'},
      {empId:104, empName:'Vamshi',  salary:4545.45, gender:'Male',   doj:'08-28-2015', country:"JAP", emailId:'vamshi@gmail.com',  password:'123'},
      {empId:105, empName:'Krishna', salary:5656.56, gender:'Male',   doj:'09-29-2014', country:"UK",  emailId:'krishna@gmail.com', password:'123'}
    ];
  }
  ngOnInit() {
   
  }
  getEmployee() {

    this.emp = null;
  
    this.employees.forEach((employee: any) => {
      if (employee.empId == this.empId) {
        this.emp = employee;
      }
    });
  }

}
